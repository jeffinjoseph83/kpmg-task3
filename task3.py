def getNestedValue(obj, key):
    if type(obj) is not dict:
        return None
    for item in key:
        if (item in obj.keys()):
            if type(obj[item]) is dict:
                obj=obj[item]
            else:
                obj=obj[item]
                return obj  
        else:
          return None
  

obj = {'a': {'b': {'c': 'd'}}}
arr=''
k="a/b/c"
for x in k:
 if (x != '/'):
   arr=arr+x
#print(arr)
value = getNestedValue(obj, arr)
print(value)
